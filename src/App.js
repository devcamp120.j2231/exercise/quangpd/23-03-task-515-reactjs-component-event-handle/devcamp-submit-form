//Import thư viện boostrap css
import "bootstrap/dist/css/bootstrap.min.css";
import Title from "./components/TittleComponent/title";
import Form from "./components/FormComponent/form";

function App() {
  return (
    <div>
      <Title/>
      <Form/>
    </div>
  );
}

export default App;
