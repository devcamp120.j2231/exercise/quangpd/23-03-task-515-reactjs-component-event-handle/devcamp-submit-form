import { Component } from "react";

class Form extends Component{
    onChangeFirstName(event){
        console.log(event.target.value);
    }

    onChangeLastName(event){
        console.log(event.target.value);
    }

    onChangeSelect(event){
        console.log(event.target.value);
    }

    onChangeTextArea(event){
        console.log(event.target.value);
    }

    onSubmitForm(){
        console.log("Form đã được submit");
    }

    render(){
        return(
            <>
                <form className="form" onSubmit={this.onSubmitForm}>
                    <div className=" container mt-3" style={{backgroundColor : "#f2f2f2" , padding : "40px"}}>
                        <div className="row mt-3">
                            <div className="col-sm-4">
                                <label>First Name</label>
                            </div>
                            <div className="col-sm-8">
                                <input type="text" onChange={this.onChangeFirstName} name="firstname" placeholder="Your first name ..."className="form-control"/>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-4">
                                <label>Last Name</label>
                            </div>
                            <div className="col-sm-8">
                                <input type="text" onChange={this.onChangeLastName} name="lastname" placeholder="Your last name.." className="form-control"/>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-4">
                                <label>Country</label>
                            </div>
                            <div className="col-sm-8">
                                <select className="form-control" onChange={this.onChangeSelect}>
                                    <option value={"Chọn Nước"}>Chọn Nước</option>
                                    <option value={"Việt Nam"}>Việt Nam</option>
                                    <option value={"Mỹ"}>Mỹ</option>
                                    <option value={"Úc"}>Úc</option>
                                    <option value={"Triều Tiên"}>Triều Tiên</option>
                                </select>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col-sm-4">
                                <label>Subject</label>
                            </div>
                            <div className="col-sm-8">
                                <textarea onChange={this.onChangeTextArea} className="form-control" placeholder="Write Something ..."></textarea>
                            </div>
                        </div>
                        <div className="row mt-4">
                            <div className="col-sm-12 text-right">
                                <input  type="submit" className="btn btn-success" value={"send Data"}/>
                            </div>
                        </div>      
                    </div>
                </form>
            </>
        )
    }
}

export default Form